#!/usr/bin/env bash
# get new version : https://cmake.org/files/v3.14/cmake-3.14.0-rc3.tar.gz
wget https://cmake.org/files/v3.14/cmake-3.14.0-rc3.tar.gz
tar xvf cmake-3.14.0-rc3.tar.gz
cd cmake-3.14.0-rc3/
./configure
make
make install
export PATH="/usr/local/bin:$PATH"
cmake .